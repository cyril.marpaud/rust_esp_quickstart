mod wifi;

use esp_idf_hal::{delay::FreeRtos, peripherals::Peripherals};
use esp_idf_svc::ping::EspPing;
use esp_idf_sys as _;

use crate::wifi::Wifi;

fn main() {
	esp_idf_sys::link_patches();

	let peripherals = Peripherals::take().expect("Failed to take peripherals");

	let wifi = Wifi::init(peripherals.modem); // Connectivity goes away when dropped

	FreeRtos::delay_ms(5000); // Wait for the DHCP server to deliver a lease

	let gw_addr = wifi
		.sta_netif()
		.get_ip_info()
		.expect("Failed to get ip info")
		.subnet
		.gateway
		.into();

	let ping = EspPing::default()
		.ping(gw_addr, &Default::default())
		.expect("Failed to ping");

	println!("Ping summary: {:?}", ping);
}
