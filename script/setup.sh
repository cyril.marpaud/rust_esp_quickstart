#!/bin/bash

echo -e "Adding universe repo\n\n"
sudo add-apt-repository -y universe

echo -e "\n\nInstalling dependencies\n\n"
sudo apt install -y clang curl git libssl-dev libudev-dev=251.4-1ubuntu7 pkg-config python3-pip

echo -e "\n\nInstalling Rust and Cargo\n\n"
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
. "$HOME/.cargo/env"

echo -e "\n\nInstalling Rust nightly toolchain\n\n"
rustup toolchain install nightly --component rust-src

echo -e "\n\nInstalling cargo modules\n\n"
cargo install espflash espmonitor ldproxy cargo-generate

echo -e "\n\nCreating a udev rule\n\n"
echo -e "SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"303a\", ATTRS{idProduct}==\"1001\", MODE=\"0660\", GROUP=\"plugdev\"" | sudo tee /etc/udev/rules.d/99-esp-rust-board.rules > /dev/null

echo -e "\n\nReloading udev rules\n\n"
sudo udevadm control --reload-rules && sudo udevadm trigger

echo -e "\n\nConnect you Rust ESP Board now.\nThen you can either :\n- generate a fresh \"Hello, World!\" project with 'cargo generate --git https://github.com/esp-rs/esp-idf-template cargo'\n- or move into your existing project's folder ('cd examples/blink', for example)\nFinally, run 'cargo run' to build, flash and execute it on the target."

